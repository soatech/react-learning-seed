# ReactJS Learning #

Basic eSpeakers React setup for learning their style of ReactJS or just ReactJS learning in general

https://youtu.be/_XbJ34_WN9s

### How do I get set up? ###

Install NodeJS [https://nodejs.org/en/](https://nodejs.org/en/).  This exists in apt (nodejs, npm) but may be outdated.  I prefer to compile from scratch.

*Tip (optional): After NodeJS is installed, install npm via npm (yo dawg): `npm install -g npm` (you may need to prefix that with `sudo` on OSX and Linux).  This will ensure you have the latest version and can more easily keep it up to date.*

You'll be asked to enter your computer password. It will not show, but it will be entered. Once the password is entered, hit return.

* Clone repo

    1.  To develop on a site locally you will need to clone the project from Bitbucket.
    2.  On your own computer, open up terminal.
    3.  Type in cd. Add space and drag in your Bitbucket folder. This will give you your Bitbucket path. Press enter.
    4. From Atlassian, select Bitbucket from the hamburger menu on the left of the Confluence logo.
    5. You will need to log into Bitbucket.
    6. Select the project you wish to deploy on your own computer.
    7. Copy the code in the upper right hand corner. e.g. git@bitbucket.org:soatech/carillu.com.git
    8. Go into terminal and type $ git clone add space, then paste git@bitbucket.org:soatech/carillu.com.git  !!!DO NOT PRESS ENTER AFTER git clone.        
    9. Press enter after you past the git@bitbucket.... code.
    10. You will see something like the following:
 
          Cloning into 'carillu.com'...

          remote: Counting objects: 20, done.

          remote: Compressing objects: 100% (14/14), done.

          remote: Total 20 (delta 5), reused 0 (delta 0)
     
          Receiving objects: 100% (20/20), done.
     
          Resolving deltas: 100% (5/5), done.

          Checking connectivity... done.
     

     11. Go into your computer's Bitbucket folder, you will see the cloned folder.


Open up terminal in Webstorm and type in:
* run `npm install`

Open up a new terminal window in Webstorm by clicking on the "+" button, type in:
* run `npm start` to compile src/app.js and src/app.less

Open up a third new terminal window in Webstorm by clicking on the "+" button, type in:
* run `npm run dev-server` to serve up public/

* Point browser to [http://localhost:3100](http://localhost:3100)

### Tutorial App ###

To see the completed tutorial app, switch to the `tutorial` branch.

### More Resources ###

And just a reminder that we do have a lot of great resources on the shelf if folk want to dive even deeper.

This overview is about the same length as mine, but broken up more and is a really great high level overview.

* ./Video/Introduction to Modern Client-Side Programming/05 - ReactJS

More detailed coverage:

* ./eBooks/ReactJS Essentials
* ./eBooks/React - Up and Running
* ./Video/A Practical Introduction to ReactJS
* ./Video/Oreilly Learning Paths/Introduction to the Modern Front-End Web/05 - A Practical Introduction to React.js by Mike Sugarbaker
* ./Video/Mastering ReactJS

Or straight from the horse's mouth.  Takes about 2 hours to read through it all, but great information and the right amount of details:
https://facebook.github.io/react/docs/why-react.html

And always hit me up for questions.