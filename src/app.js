const React = require('react');
const ReactDOM = require('react-dom');
const {createClass} = React;

const App = createClass({
    render() {
        return <h1>Hello World!</h1>;
    }
});

ReactDOM.render(<App/>, document.getElementById('react-target'));